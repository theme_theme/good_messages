var express = require('express');
var router = express.Router();
var pg = require('pg');
var path = require('path');


require('./todoController')(router);
require('./messageController')(router);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname, '..','views', 'index.html'));
  //res.render('index', { title: 'Express' });
});

router.get('/about', function(req, res, next) {
  res.sendFile(path.join(__dirname, '..','views', 'about.html'));
  //res.render('index', { title: 'Express' });
});
router.get('/add', function(req, res, next) {
  res.sendFile(path.join(__dirname, '..','views', 'add.html'));
  //res.render('index', { title: 'Express' });
});
router.get('/approve', function(req, res, next) {
  res.sendFile(path.join(__dirname, '..','views', 'approve.html'));
  //res.render('index', { title: 'Express' });
});
module.exports = router;