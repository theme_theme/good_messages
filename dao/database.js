var pg = require('pg');
var path = require('path');
var connectionString =  require(path.join(__dirname, '../', 'config'));

var client = new pg.Client(connectionString);
client.connect();
//var query = client.query('CREATE TABLE items(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)');
//var query = client.query("CREATE TYPE STATUS_TYPE AS ENUM ('NEW', 'APPROVED','REJECTED')");
query = client.query("ALTER TABLE messages DROP COLUMN created_time");
//query.on('end', function() {});
//query = client.query("CREATE TABLE messages(id SERIAL PRIMARY KEY, text VARCHAR(200) not null, author VARCHAR(200) not null, status STATUS_TYPE not null default 'NEW')");
//query = client.query("ALTER TABLE messages ADD COLUMN created_time BIGINT not null ");
query = client.query("ALTER TABLE messages ADD COLUMN created_time TIMESTAMP not null ");
query.on('end', function() { client.end(); });
