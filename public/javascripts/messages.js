angular.module('messages', [])

    .controller('messageController', function($scope, $http) {

        $scope.formData = {};
        $scope.todoData = {};
        $scope.newData = {};

        // Get all todos
        $http.get('/api/random')
            .success(function(data) {
                $scope.mData = data;
                console.log("mData:"+data.toString());
            })
            .error(function(error) {
                console.log('Error: ' + error);
            });
        // Get new messages
        $http.get('/api/new')
            .success(function(data) {
                $scope.newData = data;
                console.log(data);
            })
            .error(function(error) {
                console.log('Error: ' + error);
            });


        $scope.rejectMessage = function(messageID) {
            alert('reject');
            $http.post('/api/status/' + messageID, {status:'REJECTED'})
                .success(function(data) {
                    $scope.todoData = data;
                    console.log(data);
                })
                .error(function(data) {
                    console.log('Error: ' + data);
                });
        };
        $scope.approveMessage = function(messageID) {
            alert('approve');
            $http.post('/api/status/' + messageID, {status:'APPROVED'})
                .success(function(data) {
                    $scope.todoData = data;
                    console.log(data);
                })
                .error(function(data) {
                    console.log('Error: ' + data);
                });
        };
        // Create a new message
        $scope.createMessage = function(messageID) {
            $http.post('/api/message', $scope.formData)
                .success(function(data) {
                    $scope.formData = {};
                    $scope.todoData = data;
                    console.log(data);
                })
                .error(function(error) {
                    console.log('Error: ' + error);
                });
        };

    });
