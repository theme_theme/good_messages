var pg = require('pg');
var path = require('path');
var connectionString =  require(path.join(__dirname, '../', 'config'));

module.exports = function(router){


    router.get('/api/message', function(req, res) {

        var results = [];


        // Get a Postgres client from the connection pool
        pg.connect(connectionString, function(err, client, done) {
            // Handle connection errors
            if(err) {
                done();
                console.log(err);
                return res.status(500).send(json({ success: false, data: err}));
            }

            // SQL Query > Select Data
            var query = client.query("SELECT * FROM messages ORDER BY id ASC");

            // Stream results back one row at a time
            query.on('row', function(row) {
                results.push(row);
            });

            // After all data is returned, close connection and return results
            query.on('end', function() {
                done();
                return res.json(results);
            });
        });

    });

    router.get('/api/new', function(req, res) {

        var results = [];


        // Get a Postgres client from the connection pool
        pg.connect(connectionString, function(err, client, done) {
            // Handle connection errors
            if(err) {
                done();
                console.log(err);
                return res.status(500).send(json({ success: false, data: err}));
            }

            // SQL Query > Select Data
            var query = client.query("SELECT * FROM messages WHERE status='NEW' ORDER BY id ASC");

            // Stream results back one row at a time
            query.on('row', function(row) {
                results.push(row);
            });

            // After all data is returned, close connection and return results
            query.on('end', function() {
                done();
                return res.json(results);
            });
        });

    });

    router.post('/api/status/:message_id', function(req, res) {

        var results = [];

        // Grab data from http request
        var id = req.params.message_id;
        var data = {status: req.body.status};

        // Get a Postgres client from the connection pool
        pg.connect(connectionString, function(err, client, done) {
            // Handle connection errors
            if(err) {
                done();
                console.log(err);
                return res.status(500).json({ success: false, data: err});
            }

            client.query("UPDATE messages SET status=($1) WHERE id=($2)", [data.status, id]);

            // SQL Query > Select Data
            var query = client.query("SELECT * FROM messages where id = ($1)",[id]);

            // Stream results back one row at a time
            query.on('row', function(row) {
                results.push(row);
            });

            // After all data is returned, close connection and return results
            query.on('end', function() {
                done();
                return res.json(results);
            });


        });
    });

    router.get('/api/random', function(req, res) {
        var results = [];


        // Get a Postgres client from the connection pool
        pg.connect(connectionString, function(err, client, done) {
            // Handle connection errors
            if(err) {
                done();
                console.log(err);
                return res.status(500).send(json({ success: false, data: err}));
            }
             // SQL Query > Select Data
            var query = client.query("SELECT * FROM messages WHERE status='APPROVED' ");

            // Stream results back one row at a time
            query.on('row', function(row) {
                results.push(row);
            });
            // After all data is returned, close connection and return results
            query.on('end', function() {
                done();
                var it = Math.floor(Math.random()*results.length);
                console.log("it:"+it);
                console.log("results.length:"+results.length);
                console.log("Math.random():"+Math.random());
                //console.log("results[it]:"+Json.toString(results[it]));
                return res.json(results[it]);
            });
        });

    });

    router.get('/api/message/:message_id', function(req, res) {

        var results = [];

        // Grab data from the URL parameters
        var id = req.params.message_id;

        // Get a Postgres client from the connection pool
        pg.connect(connectionString, function(err, client, done) {
            // Handle connection errors
            if(err) {
                done();
                console.log(err);
                return res.status(500).send(json({ success: false, data: err}));
            }

            // SQL Query > Select Data
            var query = client.query("SELECT * FROM messages WHERE id=($1)", [id]);

            // Stream results back one row at a time
            query.on('row', function(row) {
                results.push(row);
            });

            // After all data is returned, close connection and return results
            query.on('end', function() {
                done();
                return res.json(results);
            });
        });

    });

    router.post('/api/message', function(req, res) {

        var results = [];

        // Grab data from http request
        var data = {text: req.body.text, author: req.body.author};

        // Get a Postgres client from the connection pool
        pg.connect(connectionString, function(err, client, done) {
            // Handle connection errors
            if(err) {
                done();
                console.log(err);
                return res.status(500).json({ success: false, data: err});
            }

            // SQL Query > Insert Data
            var time = new Date();
            client.query("INSERT INTO messages(text , author , created_time) values($1, $2, $3)", [data.text, data.author, time]);

            // SQL Query > Select Data
            var query = client.query("SELECT * FROM messages where created_time = ($1)",[time]);

            // Stream results back one row at a time
            query.on('row', function(row) {
                results.push(row);
            });

            // After all data is returned, close connection and return results
            query.on('end', function() {
                done();
                return res.json(results);
            });


        });
    });

    router.put('/api/message/:message_id', function(req, res) {

        var results = [];

        // Grab data from the URL parameters
        var id = req.params.message_id;

        // Grab data from http request
        var data = {text: req.body.text, author: req.body.author,status: req.body.status};

        // Get a Postgres client from the connection pool
        pg.connect(connectionString, function(err, client, done) {
            // Handle connection errors
            if(err) {
                done();
                console.log(err);
                return res.status(500).send(json({ success: false, data: err}));
            }

            // SQL Query > Update Data
            client.query("UPDATE messages SET text=($1), author=($2),status=($3) WHERE id=($4)", [data.text, data.complete, data.status, id]);

            // SQL Query > Select Data
            var query = client.query("SELECT * FROM messages ORDER BY id ASC");

            // Stream results back one row at a time
            query.on('row', function(row) {
                results.push(row);
            });

            // After all data is returned, close connection and return results
            query.on('end', function() {
                done();
                return res.json(results);
            });
        });

    });

    router.delete('/api/message/:message_id', function(req, res) {

        var results = [];

        // Grab data from the URL parameters
        var id = req.params.message_id;


        // Get a Postgres client from the connection pool
        pg.connect(connectionString, function(err, client, done) {
            // Handle connection errors
            if(err) {
                done();
                console.log(err);
                return res.status(500).json({ success: false, data: err});
            }

            // SQL Query > Delete Data
            client.query("DELETE FROM messages WHERE id=($1)", [id]);

            // SQL Query > Select Data
            var query = client.query("SELECT * FROM messages ORDER BY id ASC");

            // Stream results back one row at a time
            query.on('row', function(row) {
                results.push(row);
            });

            // After all data is returned, close connection and return results
            query.on('end', function() {
                done();
                return res.json(results);
            });
        });

    });
}